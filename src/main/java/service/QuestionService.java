package service;

import form.CommentForm;
import form.QuestionForm;
import models.Comment;
import models.Question;

import java.util.List;

public interface QuestionService {

    Question addQuestion(QuestionForm questionForm);

    Comment addComment(CommentForm commentForm);

    void removeQuestion(Long questionId);

    List<Question> findAllQuestions();

    Question findQuestionById(Long questionId);

    List<Comment> findCommentsByQuestionId(Long questionId);

    public List<Question> findQuestionsByUserId(Long userId);

}
