package service;

import form.CommentForm;
import form.QuestionForm;
import models.Comment;
import models.Question;
import models.User;
import repositories.CommentsRepository;
import repositories.QuestionRepository;

import java.util.List;
import java.util.Optional;

public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final CommentsRepository commentsRepository;


    public QuestionServiceImpl(QuestionRepository questionRepository, CommentsRepository commentsRepository) {
        this.questionRepository = questionRepository;
        this.commentsRepository = commentsRepository;
    }

    @Override
    public Question addQuestion(QuestionForm questionForm) {
        Question question = new Question();
        question.setTitle(questionForm.getTitle());
        question.setContent(questionForm.getContent());
        question.setUser(questionForm.getUser());

        return questionRepository.save(question);
    }

    @Override
    public Comment addComment(CommentForm commentForm) {
        Comment comment = new Comment(commentForm.getQuestionId(), commentForm.getUser(), commentForm.getContent());
        return commentsRepository.save(comment);
    }

    @Override
    public void removeQuestion(Long questionId) {
    }

    @Override
    public List<Question> findAllQuestions() {
        List<Question> questions = questionRepository.findAll();
        for (Question question : questions) {
            question.setUser(questionRepository.findUserByQuestionId(question.getId()));
            question.setComments(findCommentsByQuestionId(question.getId()));
        }
        return questions;
    }

    @Override
    public Question findQuestionById(Long questionId) {
        Optional<Question> optionalQuestion = questionRepository.findById(questionId);
        if (!optionalQuestion.isPresent()) {
            return null;
        }
        Question question = optionalQuestion.get();
        question.setUser(questionRepository.findUserByQuestionId(question.getId()));
        question.setComments(findCommentsByQuestionId(question.getId()));
        return question;
    }

    @Override
    public List<Comment> findCommentsByQuestionId(Long questionId) {
        List<Comment> commentsByQuestionId = commentsRepository.findCommentsByQuestionId(questionId);

        for (Comment comment : commentsByQuestionId) {
            comment.setUser(commentsRepository.findUserByCommentId(comment.getId()));
        }
        return commentsByQuestionId;
    }

    @Override
    public List<Question> findQuestionsByUserId(Long userId) {
        List<Question> questions = questionRepository.findQuestionsByUserId(userId);
        for (Question question : questions) {
            question.setUser(questionRepository.findUserByQuestionId(question.getId()));
            question.setComments(findCommentsByQuestionId(question.getId()));
        }
        return questions;
    }
}
