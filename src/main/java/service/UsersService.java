package service;

import form.AuthForm;
import form.UserForm;
import models.User;

import javax.servlet.http.Cookie;

public interface UsersService {
    User register(UserForm userForm);
    Cookie signIn(AuthForm authForm);
    User findUserByCookieValue(String cookieValue);

}
