package servlets;

import models.Question;
import models.User;
import service.CookieService;
import service.QuestionService;
import service.UsersService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {

    private UsersService usersService;
    private QuestionService questionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        usersService = (UsersService) config.getServletContext().getAttribute("usersService");
        questionService = (QuestionService) config.getServletContext().getAttribute("questionsService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        Cookie auth = CookieService.getCookie(request, "auth");
        if (auth != null) {

            if (request.getParameter("exit") != null) {
                CookieService.deleteCookie(request, response, "auth");
                response.sendRedirect("/main");
                return;
            }

            User user = usersService.findUserByCookieValue(auth.getValue());
            List<Question> questions = questionService.findQuestionsByUserId(user.getId());
            request.setAttribute("myQuestions", questions);
            request.setAttribute("user", user);

        }

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/profile.jsp");
        requestDispatcher.forward(request, response);

    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
