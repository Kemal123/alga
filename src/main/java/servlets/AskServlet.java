package servlets;

import form.QuestionForm;
import models.Question;
import models.User;
import service.CookieService;
import service.QuestionService;
import service.UsersService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ask")
public class AskServlet extends HttpServlet {

    private UsersService usersService;
    private QuestionService questionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        usersService = (UsersService) config.getServletContext().getAttribute("usersService");
        questionService = (QuestionService) config.getServletContext().getAttribute("questionsService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        Cookie auth = CookieService.getCookie(request, "auth");
        if (auth != null) {

            String title = request.getParameter("title");
            String text = request.getParameter("text");

            if (title != null && text != null && !title.equals("") && !text.equals("")) {

                User user = usersService.findUserByCookieValue(auth.getValue());

                QuestionForm questionForm = new QuestionForm(title, text, user);
                Question question = questionService.addQuestion(questionForm);

                request.setAttribute("id", question.getId());
                response.sendRedirect("/forum");
                return;
            }
        }
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/ask.jsp");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");

        Cookie auth = CookieService.getCookie(req, "auth");
        if (auth != null) {
            String id = req.getParameter("question_id");
            if (id != null && !id.equals("")) {
                req.getRequestDispatcher("jsp/question.jsp").forward(req, resp);
            }
        }
    }
}
