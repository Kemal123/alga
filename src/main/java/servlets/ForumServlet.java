package servlets;

import models.Question;
import models.User;
import service.CookieService;
import service.QuestionService;
import service.UsersService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/forum")
public class ForumServlet extends HttpServlet {

    private UsersService usersService;
    private QuestionService questionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        usersService = (UsersService) config.getServletContext().getAttribute("usersService");
        questionService = (QuestionService) config.getServletContext().getAttribute("questionsService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        Cookie auth = CookieService.getCookie(request, "auth");
        if (auth != null) {

            List<Question> questions = questionService.findAllQuestions();
            request.setAttribute("questions", questions);
        }

        String questionId = request.getParameter("question_id");
        if (questionId != null) {
            request.getRequestDispatcher("jsp/question.jsp").forward(request, response);
            return;
        }

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/forum.jsp");
        requestDispatcher.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
