package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import form.CommentForm;
import models.Comment;
import models.Question;
import models.User;
import service.CookieService;
import service.QuestionService;
import service.UsersService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

@WebServlet("/question")
public class QuestionServlet extends HttpServlet {

    private UsersService usersService;
    private QuestionService questionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        usersService = (UsersService) config.getServletContext().getAttribute("usersService");
        questionService = (QuestionService) config.getServletContext().getAttribute("questionsService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        ObjectMapper objectMapper = new ObjectMapper();

        Cookie auth = CookieService.getCookie(req, "auth");
        if (auth != null) {

            User user = usersService.findUserByCookieValue(auth.getValue());

            String param = req.getParameter("question_id");
            if (param != null) {
                String text = req.getParameter("text");
                if (text != null && !text.equals("")) {
                    CommentForm commentForm = new CommentForm();
                    commentForm.setContent(text);
                    commentForm.setQuestionId(Long.valueOf(param));
                    commentForm.setUser(user);

                    Comment comment = questionService.addComment(commentForm);
                }

                Question question = questionService.findQuestionById(Long.valueOf(param));
                String json = objectMapper.writeValueAsString(question);
                resp.setContentType("application/json");
                resp.getWriter().println(json);
                return;
            }
        }

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("jsp/forum.jsp");
        requestDispatcher.forward(req, resp);

    }
}
