package servlets;


import form.UserForm;
import models.User;
import repositories.UsersRepository;
import repositories.UsersRepositoryImpl;
import service.UsersService;
import service.UsersServicesImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@WebServlet("/register")
public class RegistrationServlet extends HttpServlet {

    private UsersService usersService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        usersService = (UsersService) config.getServletContext().getAttribute("usersService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("jsp/registration.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String login = request.getParameter("Login");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String retypePassword = request.getParameter("repeatPassword");
        String status = "";

        if (login.length() < 3 & login.length() < 3) {
            status = "login - не менее 3 символов!";
            request.setAttribute("validation", status);
            request.getRequestDispatcher("jsp/registration.jsp").forward(request, response);
            return;
        }

        if (email.length() == 0) {
            status = "Поле почты не должно быть пустым!";
            request.setAttribute("validation", status);
            request.getRequestDispatcher("jsp/registration.jsp").forward(request, response);
            return;
        }

        if (password.length() < 8) {
            status = "Длина пароля - не менее 8 символов!";
            request.setAttribute("validation", status);
            request.getRequestDispatcher("jsp/registration.jsp").forward(request, response);
            return;
        }


        if (password.equals(retypePassword)) {

            UserForm userForm = new UserForm();
            userForm.setLogin(login);
            userForm.setEmail(email);
            userForm.setPassword(password);

            System.out.println(userForm.getEmail());

            User user = usersService.register(userForm);
            if (user != null) {

                response.addCookie(new Cookie("email", email));
                response.sendRedirect("/signIn");
                return;
            } else {
                status = "Не удалось создать аккаунт!";
            }
        } else {
            status = "Пароли не совпадают!";
        }

        request.setAttribute("validation", status);
        request.getRequestDispatcher("jsp/registration.jsp").forward(request, response);
    }
}


