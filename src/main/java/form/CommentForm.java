package form;

import models.User;

public class CommentForm {

    private Long questionId;
    private User user;
    private String content;

    public CommentForm(Long questionId, User user, String content) {
        this.questionId = questionId;
        this.user = user;
        this.content = content;
    }

    public CommentForm() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }
}

