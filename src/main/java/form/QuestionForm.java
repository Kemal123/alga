package form;

import models.User;

public class QuestionForm {

    private String title;
    private String content;
    private User user;

    public QuestionForm(String title, String content, User user) {
        this.title = title;
        this.content = content;
        this.user = user;
    }

    public QuestionForm() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

