package form;

public class AuthForm {
    private String email;
    private String password;

    public AuthForm(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public AuthForm() {
    }

    public String getEmail() {
        return email;
    }


    public String getPassword() {
        return password;
    }

}
