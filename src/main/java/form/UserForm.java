package form;

public class UserForm {
    private String email;
    private String password;
    private String login;

    public UserForm(String email, String password, String login) {
        this.email = email;
        this.password = password;
        this.login = login;
    }

    public UserForm() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}

