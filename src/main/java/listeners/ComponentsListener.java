package listeners;

import repositories.*;
import service.QuestionService;
import service.QuestionServiceImpl;
import service.UsersService;
import service.UsersServicesImpl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ComponentsListener implements ServletContextListener {

    private final String URL = "jdbc:postgresql://localhost:5432/travel_agency";
    private final String USERNAME = "postgres";
    private final String PASSWORD = "K2002080210k";

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            UsersRepository usersRepository = new UsersRepositoryImpl(connection);
            AuthRepository authRepository = new AuthRepositoryImpl(connection);
            UsersService usersService = new UsersServicesImpl(usersRepository, authRepository);
            servletContextEvent.getServletContext().setAttribute("usersService", usersService);

            QuestionRepository questionRepository = new QuestionRepositoryImpl(connection);
            CommentsRepository commentsRepository = new CommentsRepositoryImpl(connection);
            QuestionService questionService = new QuestionServiceImpl(questionRepository, commentsRepository);
            servletContextEvent.getServletContext().setAttribute("questionsService", questionService);

        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Unavailable");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
