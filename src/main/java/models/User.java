package models;

public class User {
    private Long id;
    private String login;
    private String email;
    private String passwordHash;

    public User(Long id, String email, String passwordHash, String login) {
        this.id = id;
        this.email = email;
        this.passwordHash = passwordHash;
        this.login = login;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
