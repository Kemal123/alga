package models;

public class Comment {

    private Long id;
    private Long questionId;
    private User user;
    private String text;

    public Comment(Long id, Long questionId, User user, String text) {
        this.id = id;
        this.questionId = questionId;
        this.user = user;
        this.text = text;
    }

    public Comment(Long questionId, User user, String text) {
        this.questionId = questionId;
        this.user = user;
        this.text = text;
    }

    public Comment() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }
}
