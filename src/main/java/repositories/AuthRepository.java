package repositories;

import models.Auth;
import repositories.CrudRepository;

public interface AuthRepository extends CrudRepository<Auth> {
    Auth findByCookieValue(String cookieValue);
}