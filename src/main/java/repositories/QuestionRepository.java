package repositories;

import models.Question;
import models.User;

import java.util.List;

public interface QuestionRepository extends CrudRepository<Question> {

    List<Question> findQuestionsByUserId(Long userId);

    User findUserByQuestionId(Long questionId);

}
