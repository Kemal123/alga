package repositories;

import models.Comment;
import models.Question;
import models.User;

import java.util.List;

public interface CommentsRepository extends CrudRepository<Comment> {

    List<Comment> findCommentsByQuestionId(Long questionId);

    User findUserByCommentId(Long commentId);

}
