package repositories;

import mapper.RowMapper;
import models.Question;
import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class QuestionRepositoryImpl implements QuestionRepository {

    private final Connection connection;

    //language=sql
    private final String INSERT_QUESTION = "INSERT INTO question (user_id, title, content) VALUES (?, ?, ?)";
    private final String FIND_QUESTIONS_BY_USER_ID = "SELECT * FROM question WHERE user_id=? order by id desc;";
    private final String FIND_ALL = "SELECT * FROM question order by id desc;";
    private final String FIND_QUESTION_BY_ID = "SELECT * FROM question WHERE id=?;";
    private final String FIND_USER_BY_QUESTION_ID = "select users.id as id, login, email, password_hash from users inner join question q on users.id = q.user_id where q.id = ?;";


    public QuestionRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Question> findAll() {
        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL);
            resultSet = preparedStatement.executeQuery();
            List<Question> questions = rowMapQuestions.rowMap(resultSet);
            return questions;
        } catch (SQLException e) {
        }
        return null;
    }

    @Override
    public Optional<Question> findById(Long id) {
        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_QUESTION_BY_ID);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            return Optional.ofNullable(rowMapper.rowMap(resultSet));

        } catch (SQLException e) {
        }
        return Optional.empty();
    }

    @Override
    public Question save(Question question) {
        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUESTION, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, question.getUser().getId());
            preparedStatement.setString(2, question.getTitle());
            preparedStatement.setString(3, question.getContent());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();

            return rowMapper.rowMap(resultSet);

        } catch (SQLException e) {

        }
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public List<Question> findQuestionsByUserId(Long userId) {
        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_QUESTIONS_BY_USER_ID);
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();
            List<Question> questions = rowMapQuestions.rowMap(resultSet);
            return questions;
        } catch (SQLException e) {

        }
        return null;
    }

    @Override
    public User findUserByQuestionId(Long questionId) {
        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_BY_QUESTION_ID);
            preparedStatement.setLong(1, questionId);
            resultSet = preparedStatement.executeQuery();
            return rowMapUser.rowMap(resultSet);
        } catch (SQLException e) {

        }
        return null;
    }

    private RowMapper<Question> rowMapper = ((resultSet) -> {
        if (resultSet.next()) {
            Question question = new Question();
            question.setId(resultSet.getLong("id"));
            question.setTitle(resultSet.getString("title"));
            question.setContent(resultSet.getString("content"));
            return question;
        } else {
            return null;
        }
    });

    private RowMapper<List<Question>> rowMapQuestions = ((resultSet) -> {
        List<Question> products = new ArrayList<>();
        while (resultSet.next()) {
            Question question = new Question();
            question.setId(resultSet.getLong("id"));
            question.setTitle(resultSet.getString("title"));
            question.setContent(resultSet.getString("content"));
            products.add(question);
        }
        return products;
    });

    private RowMapper<User> rowMapUser = ((resultSet) -> {
        if (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getLong("id"));
            user.setEmail(resultSet.getString("email"));
            user.setPasswordHash(resultSet.getString("password_hash"));
            user.setLogin(resultSet.getString("login"));
            return user;
        } else {
            return null;
        }
    });
}
