package repositories;

import mapper.RowMapper;
import models.Comment;
import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CommentsRepositoryImpl implements CommentsRepository {

    private final Connection connection;

    //language=sql
    private final String INSERT_COMMENT = "insert into comment(user_id, post_id, content) VALUES (?, ?, ?)";
    private final String FIND_COMMENTS_BY_QUESTION_ID = "SELECT * FROM comment WHERE post_id=? order by id desc;";
    private final String FIND_ALL = "SELECT * FROM comment  order by id desc;";
    private final String FIND_COMMENT_BY_ID = "SELECT * FROM comment WHERE id=?;";
    private final String FIND_USER_BY_COMMENT_ID = "select users.id as id, login, email, password_hash from users inner join comment c on users.id = c.user_id where c.id = ?;";


    public CommentsRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Comment> findAll() {
        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL);
            resultSet = preparedStatement.executeQuery();
            List<Comment> comments = rowMapComments.rowMap(resultSet);
            return comments;
        } catch (SQLException e) {
        }
        return null;
    }

    @Override
    public Optional<Comment> findById(Long id) {
        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_COMMENT_BY_ID);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            return Optional.ofNullable(rowMapper.rowMap(resultSet));

        } catch (SQLException e) {
        }
        return Optional.empty();
    }

    @Override
    public Comment save(Comment comment) {
        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_COMMENT, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, comment.getUser().getId());
            preparedStatement.setLong(2, comment.getQuestionId());
            preparedStatement.setString(3, comment.getText());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();

            return rowMapper.rowMap(resultSet);

        } catch (SQLException e) {

        }
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public List<Comment> findCommentsByQuestionId(Long userId) {
        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_COMMENTS_BY_QUESTION_ID);
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();
            return rowMapComments.rowMap(resultSet);
        } catch (SQLException e) {

        }
        return null;
    }

    @Override
    public User findUserByCommentId(Long commentId) {
        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_BY_COMMENT_ID);
            preparedStatement.setLong(1, commentId);
            resultSet = preparedStatement.executeQuery();
            return rowMapUser.rowMap(resultSet);
        } catch (SQLException e) {

        }
        return null;
    }

    private RowMapper<List<Comment>> rowMapComments = ((resultSet) -> {
        List<Comment> comments = new ArrayList<>();
        while (resultSet.next()) {
            Comment comment = new Comment();
            comment.setId(resultSet.getLong("id"));
            comment.setText(resultSet.getString("content"));
            comments.add(comment);
        }
        return comments;
    });

    private RowMapper<Comment> rowMapper = ((resultSet) -> {
        if (resultSet.next()) {
            Comment comment = new Comment();
            comment.setId(resultSet.getLong("id"));
            comment.setText(resultSet.getString("content"));
            return comment;
        }
        return null;
    });

    private RowMapper<User> rowMapUser = ((resultSet) -> {
        if (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getLong("id"));
            user.setEmail(resultSet.getString("email"));
            user.setPasswordHash(resultSet.getString("password_hash"));
            user.setLogin(resultSet.getString("login"));
            return user;
        } else {
            return null;
        }
    });


}
