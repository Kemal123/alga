<%--
  Created by IntelliJ IDEA.
  User: hasis
  Date: 16.10.2021
  Time: 18:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <style>
        <%@include file="/css/registration.css"%>
    </style>
    <title>Регистрация</title>
</head>
<main>
<body>
<form action="/register" method="post" class="form">

    <div class="register-form-container">
        <h1 class="form-tittle">
            Регистрация
        </h1>
        <div class="form-fields">
            <label for="Login"><input name="Login" id="Login" required placeholder = "login"></label><br>
        </div>
        <div class="form-fields">
            <label for="password"><input name="password" id="password"required placeholder = "Пароль"  autocomplete="off"  type="password"></label><br>
        </div>
        <div class="form-fields">
            <label for="repeat password"><input name="repeatPassword" id="repeat password"required placeholder = "Повторите пароль" autocomplete="off"  type="password"></label><br>
        </div>
        <div class="form-fields">
            <label for="email"><input name="email" id="email"required placeholder = "email"></label><br>
        </div>

            <p><button type="submit">Зарегистрироваться!</button></p>
        ${validation}
    </div>
</form>


</body>
</main>

</html>
