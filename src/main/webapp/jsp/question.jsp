<%--
  Created by IntelliJ IDEA.
  User: hasis
  Date: 28.10.2021
  Time: 23:18
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>Question</title>
    <style>
        <%@include file="/css/question.css"%>
    </style>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <script src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
</head>
<header class="header">

    <div class="container">
        <div class="header_logo">
            Вопрос
        </div>
    </div>

    <div class="profile_container">
        <table class="center">
            <tr>
                <td>
                    <form class="menu-option" method="get" action="${pageContext.request.contextPath}/forum">
                        <button class="button">Форум</button>
                    </form>
                </td>
                <td>
                    <form class="menu-option" method="get" action="${pageContext.request.contextPath}/profile">
                        <button class="button">Профиль</button>
                    </form>
                </td>
            </tr>
        </table>
    </div>

    <br>

    <div class="q">
        <table class="q-table">
            <tr>
                <td id="author_name"></td>
                <th id="title"></th>
            </tr>
        </table>
        <p id="content"></p>
    </div>

    <br>

    <h4 style="text-align: center">Напишите ответ:</h4>
    <div class="q">
        <textarea id="text" placeholder="Ваш ответ" autocomplete="off"
                  name="text"></textarea>
        <p>
            <button class="button" onclick="writeAnswer()">Отправить</button>
        <p>
    </div>

    <div id="comments">
    </div>


</header>


<script>

    function getUrlParameter(sParam) {
        let sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1];
            }
        }
        return false;
    }

    $(document).ready(function () {
        let parameter = getUrlParameter("question_id");
        if (parameter != null) {
            openQuestion(parameter)
        }
    });

    function isAuthenticated() {
        let docCookies = document.cookie;
        let prefix = "auth=";
        let begin = docCookies.indexOf("; " + prefix);
        if (begin === -1) {
            begin = docCookies.indexOf(prefix);
            if (begin !== 0) return false;
        }
        return true;
    }

    function openQuestion(id) {

        if (!isAuthenticated()) {
            return;
        }

        $.ajax({
            url: '/question',           /* Куда пойдет запрос */
            method: 'post',             /* Метод передачи (post или get) */
            dataType: 'json',          /* Тип данных в ответе (xml, json, script, html). */
            data: {
                question_id: id
            },
            success: function (question) {
                let i;
                if (question == null) {
                    return;
                }
                let comments = question.comments;
                let user = question.user;

                document.getElementById('title').textContent = question.title;
                document.getElementById('author_name').textContent = user.login + ':';
                document.getElementById('content').textContent = question.content;

                let newContent = ""
                let commentsContainer = document.getElementById('comments');

                if (comments.length === 0) {
                    newContent = `<h4 style="text-align: center">Нет ответов</h4>`;
                } else {
                    newContent = `<h4 style="text-align: center">Ответы: ` + comments.length + `</h4>`;
                    for (i = 0; i < comments.length; i++) {
                        let login = comments[i].user.login;
                        let text = comments[i].text;

                        newContent += `<div class="q">
                               <p><b>` + login + `:</b></p>
                               <p>` + text + `</p>
            </div>
            <br>`;
                    }
                }
                commentsContainer.innerHTML = newContent;

            },
            error: function (question) {
            }
        })
    }


    function writeAnswer() {

        if (!isAuthenticated()) {
            return;
        }

        let id = getUrlParameter("question_id");
        let text = document.getElementById('text').value

        if (text == null || text === "") {
            return;
        }

        $.ajax({
            url: '/question',           /* Куда пойдет запрос */
            method: 'post',             /* Метод передачи (post или get) */
            dataType: 'json',          /* Тип данных в ответе (xml, json, script, html). */
            data: {
                question_id: id,
                text: text
            },
            success: function () {

                document.getElementById('text').value = ""
                let parameter = getUrlParameter("question_id");
                if (parameter != null) {
                    openQuestion(parameter)
                }
            },
            error: function () {
            }
        })
    }
</script>

</body>
</html>
