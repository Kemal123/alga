<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: hasis
  Date: 28.10.2021
  Time: 23:18
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>Forum</title>
    <style>
        <%@include file="/css/forum.css"%>
    </style>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
</head>


<header class="header">
    <div class="container">
        <div class="header_logo">
            Форум
        </div>
    </div>

    <div class="profile_container">
        <table class="center">
            <tr>
                <td>
                    <form class="menu-option" method="get" action="${pageContext.request.contextPath}/ask">
                        <button class="button">Задать вопрос</button>
                    </form>
                </td>
                <td>
                    <form class="menu-option" method="get" action="${pageContext.request.contextPath}/profile">
                        <button class="button">Профиль</button>
                    </form>
                </td>
            </tr>
        </table>
    </div>

    <br>

    <div class="questions">
        <c:forEach var="quest" items="${questions}">
            <div class="q">
                <table class="q-table">
                    <tr>
                        <td><c:out value="${quest.getUser().getLogin()}"/>:</td>
                        <th><c:out value="${quest.getTitle()}"/></th>
                    </tr>
                </table>

                <p><c:out value="${quest.getContent()}"/></p>

                <table class="q-table">
                    <tr>
                        <td>Ответы: <c:out value="${quest.getComments().size()}"/></td>
                        <td>
                            <form method="get" action="${pageContext.request.contextPath}/forum">
                                <button type="submit" name="question_id" value="${quest.getId()}">Перейти</button>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
            <br>
        </c:forEach>
    </div>

</header>


</body>
</html>
