<%--
  Created by IntelliJ IDEA.
  User: hasis
  Date: 16.10.2021
  Time: 18:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>TravelAgency</title>
    <style>
        <%@include file="/css/main.css"%>
    </style>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">


</head>

<body>

<header class="header">
    <div class="container">

        <div class="header_logo">
            Алга
            <form class="menu-option" method="get" action="${pageContext.request.contextPath}/signIn">
                <button class="button">Вход</button>
            </form>
            <form class="menu-option" method="get" action="${pageContext.request.contextPath}/register">
                <button class="button">Регистрация</button>
            </form>
        </div>

        <div class="header_inner">


        </div>

    </div>

</header>

<div class="intro">
    <div class="container_airplane">
    </div>
</div>
</body>

</html>
