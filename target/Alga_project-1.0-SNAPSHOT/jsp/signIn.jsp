<%--
  Created by IntelliJ IDEA.
  User: hasis
  Date: 16.10.2021
  Time: 18:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Input</title>
    <style>
        <%@include file="/css/input.css"%>
    </style></head>
<main>
<body>
<form action="/signIn" method="post" class="form">

    <div class="register-form-container">
        <h1 class="form-tittle">
            Вход
        </h1>
        <div class="form-fields">
            <label for="email"><input id="email" placeholder="email"  autocomplete="on" type="text" name="email"></label><br>
        </div>
        <div class="form-fields">
            <label for="password"><input id="password" placeholder="Пароль" type="password" name="password"></label><br>
        </div>
        <div class="form-buttons">
            <p><button class="button" type="submit">ВХОД</button><p>
            ${validation}
        </div>
    </div>
</form>
</body>
</main>
</html>
