<%--
  Created by IntelliJ IDEA.
  User: hasis
  Date: 28.10.2021
  Time: 23:18
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>Ask</title>
    <style>
        <%@include file="/css/question.css"%>
    </style>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <script src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
</head>
<header class="header">

    <div class="container">
        <div class="header_logo">
            Задать вопрос
        </div>
    </div>

    <div class="profile_container">
        <table class="center">
            <tr>
                <td>
                    <form class="menu-option" method="get" action="${pageContext.request.contextPath}/forum">
                        <button class="button">Форум</button>
                    </form>
                </td>
                <td>
                    <form class="menu-option" method="get" action="${pageContext.request.contextPath}/profile">
                        <button class="button">Профиль</button>
                    </form>
                </td>
            </tr>
        </table>
    </div>

    <br>

    <div class="q">
        <form action="${pageContext.request.contextPath}/ask" method="get">
            <input name="title" id="title" placeholder="Тема">
            <textarea id="text" placeholder="Ваш вопрос" autocomplete="off"
                      name="text"></textarea>
            <p>
                <button class="button" type="submit">Отправить</button>
            <p>
        </form>
    </div>

</header>


<script>

    $(document).ready(function () {
        openQuestion(${id})
    });

    function isAuthenticated() {
        let docCookies = document.cookie;
        let prefix = "auth=";
        let begin = docCookies.indexOf("; " + prefix);
        if (begin === -1) {
            begin = docCookies.indexOf(prefix);
            if (begin !== 0) return false;
        }
        return true;
    }

    function openQuestion(id) {

        if (!isAuthenticated()) {
            return;
        }
        if (id == null) {
            return;
        }

        $.ajax({
            url: '/ask',           /* Куда пойдет запрос */
            method: 'post',             /* Метод передачи (post или get) */
            dataType: 'json',          /* Тип данных в ответе (xml, json, script, html). */
            data: {
                question_id: id
            },
            success: function () {
            },
            error: function () {
            }
        })
    }
</script>
</body>
</html>
