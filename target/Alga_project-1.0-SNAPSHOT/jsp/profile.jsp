<%--
  Created by IntelliJ IDEA.
  User: hasis
  Date: 28.10.2021
  Time: 23:18
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>Profile</title>
    <style>
        <%@include file="/css/profile.css"%>
    </style>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
</head>


<header class="header">
    <div class="container">

        <div class="header_logo">
            Профиль
        </div>
    </div>


    <div class="profile_container">
        <table class="center">
            <tr>
                <td>
                    <form method="get" action="${pageContext.request.contextPath}/forum">
                        <button class="button">Форум</button>
                    </form>
                </td>
                <td>
                    <form method="get" action="${pageContext.request.contextPath}/ask">
                        <button class="button">Задать вопрос</button>
                    </form>
                </td>
            </tr>
        </table>
    </div>

    <h2 style="text-align: center; color: #7c4747;"><c:out value="${user.getLogin()}"/></h2>
    <h2 style="text-align: center; color: #7c4747;"><c:out value="${user.getEmail()}"/></h2>
    <h2 style="text-align: center; color: #7c4747;">Вопросов: <c:out value="${myQuestions.size()}"/></h2>
    <form method="get" action="${pageContext.request.contextPath}/profile">
        <button class="button" name="exit" value="1">Выйти</button>
    </form>
    <br>

    <div class="questions">
        <h2 style="text-align: center">Мои вопросы</h2>
        <c:forEach var="quest" items="${myQuestions}">
            <div class="q">
                <table class="q-table">
                    <tr>
                        <td><c:out value="${quest.getUser().getLogin()}"/>:</td>
                        <th><c:out value="${quest.getTitle()}"/></th>
                    </tr>
                </table>

                <p><c:out value="${quest.getContent()}"/></p>

                <table class="q-table">
                    <tr>
                        <td>Ответы: <c:out value="${quest.getComments().size()}"/></td>
                        <td>
                            <form method="get" action="${pageContext.request.contextPath}/forum">
                                <button type="submit" name="question_id" value="${quest.getId()}">Перейти</button>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
            <br>
        </c:forEach>
    </div>
</header>

</body>
</html>
